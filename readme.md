# This repo was created to run mutual exclusivity methods for different methods.

The running sequesnce is as follows:

1. Compute Edge Weights
2. Random Walk
3. Connected components isolarge
4. Evaluations

### For Epistasis data there are 8 different cancer types

### Remember to use arguments while running the script(s) on terminal

for example:

~~~ 
python compute_edge_weights.py -c COADREAD -x wext -g 20 -v 
~~~

-c : cancer type
-x : method
-g : mutation threshold (20,10,5)
-v : verbose 


#############################################################################

# Second Run with threshold (Apr 20, 2020)

We have done some adjustments to the code, following that, the running sequence is as follows:

1. prep_mexcowalk_data_with_threshold.py with 20,10 and 5 threshold
2. compute_edge_weight.py
~~~
python compute_edge_weights.py -c BLCA -x orig -g 5 -v
~~~
etc etc
3. Random walk
Connected components Isolarge
4. ROC
