#!/usr/bin/env python

from utils import *
import networkx as nx
import os
import argparse
import numpy as np
from tqdm import tqdm,trange

#compute mutex values
def compute_mutex_nsep():
    if args.verbose:
        print "computing mutex values..."

    G = nx.Graph()

    for e in edge_list:
         G.add_edge(e[0], e[1])

    G.to_undirected()
    G.remove_edges_from(G.selfloop_edges())

    #num_samples = len(load_patients_to_indices())  # number of patients

    weight_out_file = mex_nsep_out_file
    fhout = open(weight_out_file, 'w+')

    count_1s = 0
    count_0s = 0

    for i in trange(len(edge_list)):
        e = edge_list[i]
        gene1_index = e[0]
        gene2_index = e[1]
        gene1 = id_to_gene[gene1_index]
        gene2 = id_to_gene[gene2_index]
	
	if gene1 in unique_genes and gene2 in unique_genes:

	    gene1_neighbours = G[gene1_index]
	    gene2_neighbours = G[gene2_index]
    
	    union_set1 = set()
	    union_set2 = set()
	    count1 = 0
	    count2 = 0
    
	    if gene1 in data:
		union_set1 = set(data[gene1])
		count1 = len(data[gene1])
	    if gene2 in data:
		union_set2 = set(data[gene2])
		count2 = len(data[gene2])
    
	    for gene in gene1_neighbours:
		if id_to_gene[gene] in data and id_to_gene[gene] != gene2_index:  # exclude gene2 ?
		    union_set1 = union_set1.union(data[id_to_gene[gene]])
		    count1 += len(data[id_to_gene[gene]])
	    for gene in gene2_neighbours:
		if id_to_gene[gene] in data and id_to_gene[gene] != gene1_index:
		    union_set2 = union_set2.union(data[id_to_gene[gene]])
		    count2 += len(data[id_to_gene[gene]])
    
    
    
	    union_set1 = len(union_set1)
	    union_set2 = len(union_set2)
    
	    m1 = 0
	    m2 = 0
	    if count1 != 0:
		m1 = float(union_set1) / count1
	    if count2 != 0:
		m2 = float(union_set2) / count2
    
	    mutex = (m1+m2)/2
    
	    if mutex == 1:
	       count_1s += 1
    
	    print >>fhout, id_to_gene[e[0]]+ "\t" + id_to_gene[e[1]] + "\t" + str(mutex)

    #print (count_0s)

    fhout.close()

def compute_mutex_from_pvalues(mutex_infile, mutex_one_threshold = pow(10,-10)):
    '''For normalizing mutual exclusivity pvalues'''
    
    if args.verbose:
	print "computing mutual exc. from pvalues"
	
    weight_out_file = mex_pval_out_file
    fhout = open(weight_out_file, 'w+')    
    
    d_all = {}
    dict_pval = {}
    dict_pval_one = {}
    
    # save lines in a dict for quick lookup
    with open(mutex_infile, 'r') as f:
	for line in f.readlines()[1:]:
	    line = line.split()
	    g1 = line[1]
	    g2 = line[2]
	    p = float(line[3])
	    
	    d_all[g1+'\t'+g2] = p
	    
    # normalization for intact edges only
    for g1_index,g2_index in tqdm(edge_list):
	g1 = id_to_gene[g1_index]
	g2 = id_to_gene[g2_index]
	
	if g1+'\t'+g2 in d_all:
	    p = d_all[g1+'\t'+g2]

	    if p < mutex_one_threshold:
		dict_pval_one[g1+'\t'+g2] = 1.0
	    else:
		dict_pval[g1+'\t'+g2] = -np.log(p)

	elif g2+'\t'+g1 in d_all:
	    p = d_all[g2+'\t'+g1]

	    if p < mutex_one_threshold:
		dict_pval_one[g1+'\t'+g2] = 1.0
	    else:
		dict_pval[g1+'\t'+g2] = -np.log(p)
		
	
    max_logpval = max(dict_pval.values())
    if args.verbose:
	print('Max -log(p): ', max_logpval)
    
    for g1_index,g2_index in edge_list:
	g1 = id_to_gene[g1_index]
	g2 = id_to_gene[g2_index]
		    
	if g1+'\t'+g2 in dict_pval_one:
	    mutex_pval = 1.0
	    print >>fhout, g1+ "\t" + g2 + "\t" + str(mutex_pval)
	
	
	elif g1+'\t'+g2 in dict_pval:
	    mutex_pval = dict_pval[g1+'\t'+g2]/max_logpval
	    print >>fhout, g1+ "\t" + g2 + "\t" + str(mutex_pval)  
	    

#compute coverage values
def compute_cov():
    if args.verbose:
        print "computing coverage values..."

    G = nx.Graph()

    for e in edge_list:
         G.add_edge(e[0], e[1])

    G.to_undirected()
    G.remove_edges_from(G.selfloop_edges())

    weight_out_file = cov_out_file
    fhout = open(weight_out_file, 'w+')
    
    count = 0
    for i in range(len(edge_list)):
        e = edge_list[i]
        gene1_index = e[0]
        gene2_index = e[1]
        gene1 = id_to_gene[gene1_index]
        gene2 = id_to_gene[gene2_index]
	
	if gene1 in unique_genes and gene2 in unique_genes:
	    gene1_patients = []
	    gene2_patients = []
    
	    if gene1 in data:
		gene1_patients = data[gene1]
	    if gene2 in data:
		gene2_patients = data[gene2]
    
	    gene1_count = len(gene1_patients)
	    gene2_count = len(gene2_patients)
	    
    
	    if gene1_count + gene2_count != 0:
		    
		gene1_cover = float(gene1_count) / num_samples
		gene2_cover = float(gene2_count) / num_samples
    
		cov = gene1_cover * gene2_cover
    
		print >>fhout, id_to_gene[e[0]]+ "\t" + id_to_gene[e[1]] + "\t" + str(cov)

    fhout.close()

def get_cutoff_for_removing_edges(orig_file,pval_file,mex_threshold=0.7):
    '''remove edges from mexcowalk that dont pass threshold
    '''
    orig_scores = {}
    with open(orig_file, "r") as f:
	lines = f.readlines()
	for line in lines:
	    line = line.strip().split()
	    orig_scores[line[0]+" "+line[1]] = float(line[2])    

    
    pval_scores = {}
    with open(pval_file, "r") as f:
	lines = f.readlines()
	for line in lines:
	    line = line.strip().split()
	    pval_scores[line[0]+" "+line[1]] = float(line[2])    
    
    ##assertion to make sure the edge lengths are same
    print (pval_file, len(orig_scores),len(pval_scores)) 
    #assert len(orig_scores) ==len(pval_scores)
    
    #get removed edges from orig
    edges_removed = len([v for v in orig_scores.values() if v<mex_threshold])
    edges_kept = len([v for v in orig_scores.values() if v>=mex_threshold])
    
    #return cutoff
    return sorted(pval_scores.values())[edges_removed]
    
    

#compute edge weights as product of mutex and coverage values with set threshold
def compute_edge_weights(key,cutoff):
    if args.verbose:
        print "Assigning edge weights..."

    # N = len(genes)

    if ('orig_' in key or 'cov_' in key) and 'mexcowalk' in key :
	mutex_pval_scores = {}
	with open(mex_nsep_out_file, "r") as f:
	    lines = f.readlines()
	    for line in lines:
		line = line.strip().split()
		mutex_pval_scores[line[0]+" "+line[1]] = float(line[2])
    else:
	mutex_pval_scores = {}
	with open(mex_pval_out_file, "r") as f:
	    lines = f.readlines()
	    for line in lines:
		line = line.strip().split()
		mutex_pval_scores[line[0]+" "+line[1]] = float(line[2])    

    cov_scores = {}
    with open(cov_out_file, "r") as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip().split()
            cov_scores[line[0]+" "+line[1]] = float(line[2])

    weight_out_file = edge_out_file
    fhout = open(weight_out_file, 'w+')

    for i in range(len(edge_list)):
        e = edge_list[i]
        gene1_index = e[0]
        gene2_index = e[1]
        gene1 = id_to_gene[gene1_index]
        gene2 = id_to_gene[gene2_index]
        gene1_patients = []
        gene2_patients = []

        if gene1 in data:
            gene1_patients = data[gene1]
        if gene2 in data:
            gene2_patients = data[gene2]

        gene1_count = len(gene1_patients)
        gene2_count = len(gene2_patients)

        if gene1_count + gene2_count != 0 and gene1+" "+gene2 in mutex_pval_scores:

	    if key==c+'_orig_mexcowalk_t' + str(mut_threshold)+'_'+str(threshold):
		mutex = mutex_pval_scores[gene1 + " " + gene2]
		cov = cov_scores[gene1 + " " + gene2]
		if mutex<threshold:
		    mutex=0.0
		res = mutex * cov
	    
	    elif key==c+'_' + method + '_mexcowalk_erm_t' + str(mut_threshold) or key==c+'_' + method + '_mexcowalk_erd_t' + str(mut_threshold):
		mutex = mutex_pval_scores[gene1 + " " + gene2]
		cov = cov_scores[gene1 + " " + gene2]
		if mutex<cutoff:
		    mutex=0.0		
		res = mutex * cov
		
	    elif key==c+'_' + method + '_mexcowalk_erd_t' + str(mut_threshold) +'_' + str(mutex_max_threshold_for_pvals_normalization):
		mutex = mutex_pval_scores[gene1 + " " + gene2]
		cov = cov_scores[gene1 + " " + gene2]
		if mutex<cutoff:
		    mutex=0.0		
		res = mutex * cov	    
	    

	    #elif 'mexcowalk' in key and method in key:
                #mutex = mutex_pval_scores[gene1 + " " + gene2]
                #cov = cov_scores[gene1 + " " + gene2]
		#if mutex<threshold:
		    #mutex=0.0		
                #res = mutex * cov
		
	    elif key==c+ '_cov_mexcowalk_t' + str(mut_threshold):
                cov = cov_scores[gene1 + " " + gene2]
                res = cov	    
		
	    
		
	    
	    print >> fhout, id_to_gene[e[0]] + "\t" + id_to_gene[e[1]] + "\t" + str(res)

    fhout.close()


if __name__ == '__main__':

    # parse arguments
    description = "Compute mutex and coverage scores and set their product as edge weights (heat)"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-m', "--model", type=str, required=False, default='mexcowalk', help="model to assign heat")
    parser.add_argument('-x', "--method", type=str, required=False, default='wext', help="mutual exclusivity model used")
    parser.add_argument('-c', "--cancer_type", type=str, required=False, help="cancer type")
    parser.add_argument('-t', '--threshold', type=float, required=False, default=0.0, help="mutex_nsep_threshold")
    parser.add_argument('-g', '--mutation_threshold', type=int, required=False, default=5, help="mutation threshold")
    parser.add_argument('-v', '--verbose', action='store_true', help='verbose')

    args = parser.parse_args()

    #model = args.model
    method = args.method
    #c = args.cancer_type
    #threshold = args.threshold
    mut_threshold = args.mutation_threshold

    # creating path
    path_pre = '../out/edge_weights/pre/'
    main_path = '../out/edge_weights/'
    data_path = '../data/'
    if not os.path.exists(path_pre):
        os.makedirs(path_pre)

    #load data pre
    id_to_gene = load_id_to_gene()
    gene_to_id = load_gene_to_id()  # gene string to indices
    edge_list = load_edge_list()
    
    

    #models_list = [method + '_mexcowalk']
    cancer_subtypes = ['BLCA', 'BRCA', 'COADREAD', 'LUAD', 'LUSC', 'SKCM', 'STAD' ,'UCEC']
    mex_methods = ['discover','fishers', 'wext']
    
    #load threshold
    threhsold_file = '../data/edge_thresholds_based_on_hint_mexcowalk_density_loss.txt'
    with open(threhsold_file) as f:
	d_threshold = {line.split()[0]:float(line.split()[1]) for line in f.readlines()[1:]}
    mutex_max_threshold_for_pvals_normalization = 0.05

    #models = [c + '_' + m for c in cancer_subtypes for m in models_list]


    #for model in models:
    #c = model.split('_')[0]
    
    for c in tqdm(cancer_subtypes):
	threshold=d_threshold[c]
	if c== 'UCEC':
	    
	    # define input paths
	    if method == 'orig':
		model = c+'_' + method + '_mexcowalk_t' + str(mut_threshold)+'_'+str(threshold) #mutex and orig models erd: edge removed based on density
	    elif method=='cov':
		model = c+'_' + method + '_mexcowalk_t' + str(mut_threshold) #mutex and orig models erd: edge removed based on density		
	    else:
		model = c+'_' + method + '_mexcowalk_erd_t' + str(mut_threshold)+ '_' + str(mutex_max_threshold_for_pvals_normalization) #mutex and orig models erd: edge removed based on density
	    
	    print model
	    # input files
	    mex_nsep_out_file = path_pre + c +'_mutex_nsep_t'+str(mut_threshold)+'.txt'
	    
	    if method=='discover':
		mex_pval_infile= data_path +method + '_mutation_filtered_ep_data/' + c + '_' + method + '_result_mutations_all_genes_'+"q1.0_normal"+ '_' +str(mut_threshold)+'.txt'
	    
	    elif method=='fishers':
		mex_pval_infile= '../../version yi - FISHERS EXACT/out/' +method + '_mutation_filtered_ep_data/' + c + '_' + method + '_result_mutations_all_genes_' + str(mut_threshold)+'.txt'
	    elif method=='wext':
		mex_pval_infile= '../../version yiv - WEXT/out/' +method + '_mutation_filtered_ep_data/' + c + '_' + method + '_result_mutations_all_genes_' + str(mut_threshold)+'.txt'
	    elif method == 'orig':
		pass
	    else:
		mex_pval_infile= data_path +method + '_mutation_filtered_ep_data/' + c + '_' + method + '_result_mutations_all_genes_' + str(mut_threshold)+'.txt'  
	    
	    if mutex_max_threshold_for_pvals_normalization == 1e-10:
		mex_pval_out_file = path_pre + c + '_' + method + '_mutex_pval_t'+ str(mut_threshold)+'.txt' 
	    else:
		mex_pval_out_file = path_pre + c + '_' + method + '_mutex_pval_t'+ str(mut_threshold)+ '_' + str(mutex_max_threshold_for_pvals_normalization)+'.txt' 	    
		
	    cov_out_file = path_pre + c + '_cov_t'+str(mut_threshold)+'.txt'
	    edge_out_file = main_path + model + '.txt'
	
	
	    # load data
	    num_samples = len(load_patients_to_indices(data_path + 'mutation_filtered_patient_indices_all_genes_ep_data/' + c + '_patient_indices.txt')) # number of patients
	    if mut_threshold==0:
		data = load_gene_vs_patient_data(data_path + 'mutation_filtered_gene_vs_patients_all_genes_ep_data' + '/' + c + '.txt')
	    else:
		data = load_gene_vs_patient_data(data_path + 'mutation_filtered_gene_vs_patients_t'+str(mut_threshold)+'_genes_ep_data' + '/' + c + '.txt')	   
	    
	    unique_genes = set(data)
	    
	    
	    #computing edge weights
	    if method == 'orig':
		compute_mutex_nsep()
	    else:
		compute_mutex_nsep()
		compute_mutex_from_pvalues(mex_pval_infile,mutex_one_threshold=mutex_max_threshold_for_pvals_normalization)
	    compute_cov()
	    
	    #using threshold we find the n number of edges removed from mutex nsep.
	    #then we find the cutoff to eliminate the same n+p number of edges from the mex methods
	    #p is the additional edges with same mex value as p
	    cutoff=0
	    # get cutoff
	    if method in mex_methods: 
		cutoff = get_cutoff_for_removing_edges(mex_nsep_out_file,mex_pval_out_file, mex_threshold=threshold)
	    print(cutoff)
	    
	    compute_edge_weights(model,cutoff)
