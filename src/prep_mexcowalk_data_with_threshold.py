#!/usr/bin/env python

import numpy as np
import pandas as pd
from tqdm import tqdm
import os


cancer_types = ['BLCA', 'BRCA', 'COADREAD', 'LUAD', 'LUSC', 'SKCM', 'STAD', 'UCEC']
threshold = [5]

for t in threshold:
    for c in cancer_types:
        
        ## file i/o        
        infile_mut = '../data/binary_matrices_all_genes_ep_mutation_filtered/{}_TML_binary_sm.txt'.format(c)
        infile_idx = '../data/mutation_filtered_patient_indices_all_genes_ep_data/{}_patient_indices.txt'.format(c)
        infile_freq = '../data/mutation_filtered_mutation_freq_all_genes_ep_data/{}_freq.txt'.format(c)
        outpath_mut = '../data/mutation_filtered_gene_vs_patients_t{}_genes_ep_data/'.format(t)
        outpath_freq = '../data/mutation_filtered_mutation_freq_t{}_genes_ep_data/'.format(t) 
        
        if not os.path.exists(outpath_mut):
            os.makedirs(outpath_mut)
        if not os.path.exists(outpath_freq):
            os.makedirs(outpath_freq)
        
        #load data from binary matrices
        df = pd.read_csv(infile_mut, sep='\t', index_col=0)
        
        
        df.drop('y',1,inplace=True)
        print(df.shape)
        print(df.head())
        
        df.drop([col for col, val in df.sum().iteritems() if val <= t], axis=1, inplace=True)
        print(df.shape)
        #print(df.head())
        
        #Gene and patients
        patients = df.index.tolist()
        genes = df.columns.tolist()
        
        # load patient indices
        with open(infile_idx, 'r') as f:
            dict_pidx = {line.split()[0]:int(line.split()[1]) for line in f.readlines()}
            
        # load freq
        with open(infile_freq, 'r') as f:
            dict_freq = {line.split()[0]:float(line.split()[1]) for line in f.readlines()}
        
        # prep data for mexcowalk input (gene vs patients and freq)
        dict_pvg = {p:set() for p in patients}
        dict_gvp = {g:set() for g in genes}
        dict_gvpidx = {g:set() for g in genes}    
        
        for g in df.columns:

            for p,v in zip(df[g].index.tolist(),df[g].tolist()):
                if v ==1:
                    dict_gvp[g].update([p])
                    dict_gvpidx[g].update([dict_pidx[p]])
                    dict_pvg[p].update([g])    
        
        outfile_mut = outpath_mut +c+'.txt'
        outfile_freq = outpath_freq +'{}_freq.txt'.format(c)    
        
        with open(outfile_mut, 'w')as f:
            for g in sorted(dict_gvpidx):
                f.write(g)
                
                for v in sorted(dict_gvpidx[g]):
                    f.write('\t' + str(v))
                f.write('\n')
                
        with open(outfile_freq, 'w')as f:
            for g in sorted(genes):
                f.write('{}\t{}\n'.format(g,dict_freq[g]))