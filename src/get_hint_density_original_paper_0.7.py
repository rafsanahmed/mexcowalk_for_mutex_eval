#!/usr/bin/env python

from utils import *
import scipy.sparse as sp
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

def get_density(nodes,edges):
    return 2*edges/(nodes*(nodes-1))
infile = '../data/mexcowalk_paper_hint_mutex_nsep.txt'
threhsold=0.7

df = pd.read_csv(infile, sep='\t', header=None)

genes_orig = set(df[0].tolist()+df[1].tolist())
print(len(genes_orig))

df2 = df[df[2]>=0.7]
print(len(df),len(df2))

genes_07 = set(df2[0].tolist()+df2[1].tolist())
print(len(genes_orig),len(genes_07))

density_orig = get_density(len(genes_orig),len(df))
density_07= get_density(len(genes_07),len(df2))
print(density_orig,density_07)
print('percentage loss',(density_orig-density_07)/density_orig)