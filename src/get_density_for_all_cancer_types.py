#!/usr/bin/env python

from utils import *
import scipy.sparse as sp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

DENSITY_HINT_MEXCOWALK_00 = 0.0008376374274305648
DENSITY_HINT_MEXCOWALK_07 = 0.0008077701992982115
PERC_LOSS_HINT_MEXCOWALK = 0.03565651098467558


def compute_density(n,e):
    '''n:nodes, e:edges'''
    e=float(e)
    n=float(n)
    return 2*e/(n*(n-1))

cancer_types = ['BLCA', 'BRCA', 'COADREAD', 'LUAD', 'LUSC', 'SKCM', 'STAD' ,'UCEC']
t=5
mut_thresholds=[round(v,2) for v in np.linspace(0.0,1,101)] #the thresholds to be tested
print(mut_thresholds)

outfile = '../data/edge_thresholds_based_on_hint_mexcowalk_density_loss.txt'
f = open(outfile,'w+')
f.write('cancer_type\tthreshold\tedges\tgenes\tdensity\tdensity_loss\n')
for c in cancer_types:
    d_threshold = {}
    diff_range = 1e-2 # the accepted range of the decsity loss, ex: diff +- 1e-2
    if True:#c=='UCEC':
        d_vals = {}
        print(c)
        infile = '../out/edge_weights/pre/{}_mutex_nsep_t{}.txt'.format(c,t)
        
        df = pd.read_csv(infile, sep='\t', header=None)
        print(df.head())
        
        #get density from df
        edges_orig = len(df)
        genes_orig = len(set(df[0].tolist()+ df[1].tolist()))
        
        density_orig = compute_density(genes_orig, edges_orig)
        print('orig_0.0',edges_orig,genes_orig,density_orig)
        
        #get density loss for each threshold from 0.01-1
        for threshold in mut_thresholds:
            df2 = df[df[2]>=threshold]
            edges = len(df2)
            genes = len(set(df2[0].tolist()+ df2[1].tolist()))
            
            if genes!=0:
                density = compute_density(genes, edges)
                diff=(density_orig-density)/density_orig
                
                if diff<PERC_LOSS_HINT_MEXCOWALK+diff_range and diff> PERC_LOSS_HINT_MEXCOWALK-diff_range:
                    print(threshold, edges,genes, density, diff)
                    d_vals[(threshold,edges,genes,density)]=diff
            else:
                print(threshold, edges,genes,None)
        
        if len(d_vals)==0:
            raise Exception('ERR! No density within +- {} range was found'.format(diff_range))
        elif len(d_vals)==1:
            k = list(d_vals)[0]
            f.write(c+'\t'+'\t'.join([str(v) for v in k]) + '\t' + str(d_vals[k]) + '\n')
        else:
            vals =list(d_vals.values())
            min_val=vals[np.abs([v - PERC_LOSS_HINT_MEXCOWALK for v in vals]).argmin()]
            print(min_val)
            k=list(d_vals.keys())[list(d_vals.values()).index(min_val)]
            print(k)
            print(c,k,d_vals[k])
            f.write(c+'\t'+'\t'.join([str(v) for v in k]) + '\t' + str(d_vals[k]) + '\n')
        
                    
